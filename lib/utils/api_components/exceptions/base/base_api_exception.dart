abstract class BaseApiException {
  final String message;

  BaseApiException({
    required this.message,
  });

  @override
  String toString() => message;
}
